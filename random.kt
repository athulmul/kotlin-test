 fun main(){
     val dice1 = Dice()
     val rolledNum = dice1.roll()

     println("rolled num: ${rolledNum}")
     println("sides: ${dice1.sides}")
 }
 
 class Dice{
     var sides = 6
     fun roll(): Int {
         val randomNumber = (1..sides).random()
         return randomNumber
     }

 }
